+++
title = 'Gallery'
date = 2024-03-02T13:00:44-08:00
draft = false
+++
#### Prof. M.P. Manmadhan inaugurating Poornodaya Book Trust on 15.10.1988. R. Reghu, K.P. Madhavan Nair, Prof. G. Kuamara Pilla, K. Parameswara Sarma are on the stage.
![Prof. M.P. Manmadhan inaugurating Poornodaya Book Trust on 15.10.1988. R. Reghu, K.P. Madhavan Nair, Prof. G. Kuamara Pilla, K. Parameswara Sarma are on the stage.](/gallery/inauguration.jpg)

#### 1988 October 15 K.P. Madhavan Nair speaking on the inaugural function. Seated on the stage are Prof. M. P. Manmathan, Prof. G. Kumara Pilla, Lukose Benjamin (Secretary, Gandhi Smaraka Nidhi), C. P. Sreedharan, etc.
![1988 October 15 K.P. Madhavan Nair speaking on the inaugural function. Seated on the stage are Prof. M. P. Manmathan, Prof. G. Kumara Pilla, Lukose Benjamin (Secretary, Gandhi Smaraka Nidhi), C. P. Sreedharan, etc.](/gallery/photo.jpg)

#### Pavizham Madhavan Nairis releasing the book M.P.ManmathanEnnaManushyan by giving the first copy to P.J. Kurien. Former Minister A.M.Thomas, Prof. G.Kumara Pilla, Prof. P.K.B. Nair, etc. are on the stage.
![Pavizham Madhavan Nairis releasing the book M.P.ManmathanEnnaManushyan by giving the first copy to P.J. Kurien. Former Minister A.M.Thomas, Prof. G.Kumara Pilla, Prof. P.K.B. Nair, etc. are on the stage.](/gallery/photo1.jpg)

#### 2002 September 17 The then Chief Minister of KeralaA.K.Antony is releasing the book G.Kumara Pilla:AatmavinteAyalkkaran by presenting the first copy to P.Viswambharan. Seated on the dais are the Former Minister P.P.George, Dr. Sukumar Azhikode, Prof. K.G.Sankara Pilla, K. Parameswara Sarma, etc.
![2002 September 17 The then Chief Minister of KeralaA.K.Antony is releasing the book G.Kumara Pilla:AatmavinteAyalkkaran by presenting the first copy to P.Viswambharan. Seated on the dais are the Former Minister P.P.George, Dr. Sukumar Azhikode, Prof. K.G.Sankara Pilla, K. Parameswara Sarma, etc.](/gallery/photo3.jpg)

#### 1997 November 22 NavjeevanTrust Chairman Biharibhai is releasing the book Mahatma Gandhis Selected Works (Five volumes) by presenting the first copy to Cardinal Rev. Dr. Mar Varkey Vithayathil. Seated on the stage are Justice P. JanakiAmma, Navjeevan Trust Managing Trustee Jithendra Desai, etc.
![1997 November 22 NavjeevanTrust Chairman Biharibhai is releasing the book Mahatma Gandhis Selected Works (Five volumes) by presenting the first copy to Cardinal Rev. Dr. Mar Varkey Vithayathil. Seated on the stage are Justice P. JanakiAmma, Navjeevan Trust Managing Trustee Jithendra Desai, etc.](/gallery/photo4.jpg)

#### 2001 July 18 : Justice Chandrasekhar Darmadhikari is releasing the book Woman-ManRelations:A GandianApproach authored by Dada Dharmadhikari by presenting the first copy to Parivragik Rajamma. Gandhi Smarakanidhi Chairman P. Gopinathan Nair looks on.
![2001 July 18 : Justice Chandrasekhar Darmadhikari is releasing the book Woman-ManRelations:A GandianApproach authored by Dada Dharmadhikari by presenting the first copy to Parivragik Rajamma. Gandhi Smarakanidhi Chairman P. Gopinathan Nair looks on.](/gallery/photo5.jpg)

#### Dr. N. Radhakrishnan is releasing the Malayalam translation of the book Life Of Mahatma Gandhi authored by Louis Fisher by presenting the first copy to C. Uthama Kuruppu.
![Dr. N. Radhakrishnan is releasing the Malayalam translation of the book Life Of Mahatma Gandhi authored by Louis Fisher by presenting the first copy to C. Uthama Kuruppu.](/gallery/photo6.jpg)

#### The Office bulding of Poornodaya Book Trust in Kochi
![The Office bulding of Poornodaya Book Trust in Kochi](/gallery/photo7.jpg)

#### 2001 October 15  The Chairperson Justice P. JanakiAmma is inaugurating Poornodaya Bhavan. Beside her on the stage are T.O. George, Dr. HenryAustin, Justice P.K. Shamsudhin, Dr. N. Radhakrishnan, M. Surendran and others.
![2001 October 15  The Chairperson Justice P. JanakiAmma is inaugurating Poornodaya Bhavan. Beside her on the stage are T.O. George, Dr. HenryAustin, Justice P.K. Shamsudhin, Dr. N. Radhakrishnan, M. Surendran and others.](/gallery/photo8.jpg)

#### 2002 June 14 : Justice V.R. Krishna Iyer is releasing the first copy of the book Gandhiji: Navothana Daarsanikan authored by Dr. K.S.Radhakrishnan, by presenting the first copy to Dr. N. Radhakrishnan. On the stage are Dr. K.S. Radhakrishnan, Rev. Father Dr. Paul Thelakkad, etc.
![2002 June 14 : Justice V.R. Krishna Iyer is releasing the first copy of the book Gandhiji: Navothana Daarsanikan authored by Dr. K.S.Radhakrishnan, by presenting the first copy to Dr. N. Radhakrishnan. On the stage are Dr. K.S. Radhakrishnan, Rev. Father Dr. Paul Thelakkad, etc.](/gallery/photo9.jpg)

#### 9. 3. 2013, Thrissur : P. Viswambharan is releasing the book A Corruption-Free Society, In Gandhijis Perspective, a collection of some 20 articles written by University students, by presenting the first copy to Dr. K.Aravindakshan.
![9. 3. 2013, Thrissur : P. Viswambharan is releasing the book A Corruption-Free Society, In Gandhijis Perspective, a collection of some 20 articles written by University students, by presenting the first copy to Dr. K.Aravindakshan.](/gallery/photo10.jpg)

#### 2002 October 17 : The former Supreme Court Chief Justice M.N. Venkitachaliah is releasing the book Gandhis Concept of Truth and Justice written byAdv. Cherian Gudallur. Seated on the dais are Dr. K.S. Radhakrishnan, Dr. SukumarAzhikode, Justice Cyriac Joseph and others.
![2002 October 17 : The former Supreme Court Chief Justice M.N. Venkitachaliah is releasing the book Gandhis Concept of Truth and Justice written byAdv. Cherian Gudallur. Seated on the dais are Dr. K.S. Radhakrishnan, Dr. SukumarAzhikode, Justice Cyriac Joseph and others.](/gallery/photo11.jpg)

#### 2004 April 4 : The former Supreme Court Justice K. T. Thomas speaking in the function organized in connection with the release of Christ In Hindus View. Swami Sakrananda, Mar Aprem Methrapolitha, I.M. Velayudhan Master, Prof. Tony Mathew etc. on the stage.
![2004 April 4 : The former Supreme Court Justice K. T. Thomas speaking in the function organized in connection with the release of Christ In Hindus View. Swami Sakrananda, Mar Aprem Methrapolitha, I.M. Velayudhan Master, Prof. Tony Mathew etc. on the stage.](/gallery/photo12.jpg)

#### 2007 January 23 The release function of the book Marakkatha Anuyathrakal written by P. Viswambharan. On the stage are Punathil Kunhabdulla, Kesavakaranavar, Education Minister M.A. Baby, P. Viswambharan, etc.
![2007 January 23 The release function of the book Marakkatha Anuyathrakal written by P. Viswambharan. On the stage are Punathil Kunhabdulla, Kesavakaranavar, Education Minister M.A. Baby, P. Viswambharan, etc.](/gallery/photo13.jpg)

#### 2005 August: The exhibition-van got ready by Poornodaya Book Trust for the exhibition/sales of Gandhian literature. Bishop Mar James Pazhayattil is inaugurating in St. Mary's Higher Secondary School, Irinjalakkuda.
![2005 August: The exhibition-van got ready by Poornodaya Book Trust for the exhibition/sales of Gandhian literature. Bishop Mar James Pazhayattil is inaugurating in St. Mary's Higher Secondary School, Irinjalakkuda.](/gallery/photo14.jpg)
