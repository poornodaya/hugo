+++
title = 'Achievements'
date = 2024-03-02T13:00:28-08:00
draft = false
+++


# Achievements

The Trust has published 118 titles of which many have gone into several editions. They are original writings, translations of books written by Gandhiji and those of Jayaprakash Narayan, Vinoba Bhave and Dada Dharmadhikari.

Gandhiji’s Autobiography (Malayalam) published by Navajivan is distributed through Poornodaya. Within a short period of 12 years the sale of it has exceeded six lakh copies.This is a record sale amoung Indian languages.

Poornodaya has published Malayalam translations of selected works of Mahathma Gandhi in 5 volums and 28 other books jointly with Navajivan Trust, Ahmedabad.

For the proper functioning and effective management of the institution, we acquired a piece of land along with a building worth ` 7o lakhs in the heart of Kochi citi. The money was raised through interest free loans and donations from well wishers.

The Trust has provided books with 50% subsidy to Gandhian organizations and the schools where they conduct Taking Gandhi to School programme. During 2011-12 Taking Gandhi to School programme is organized in nearly 800 schools and enrolled about one lakh students in Thrissur, Alappuzha, Malappuram, Kozhikode and Kasaragod districts, with the assistance of the various Gandhian institutions. As a part of this programme, we organized mobile exhibitions and sale of Gandhian literature in schools, colleges and various institutions. During the last 11 years we have been organizing mobile exhibition and sale of literature in nearly 6000 institutions all over Kerala.

The Trust has published a quarterly magazine ‘Poornodaya’ in Malayalam since 1992 and from July 2007 this is converted to a magazine. The total number of the subscribers are 7000. It treats only Gandhian, secular, sarvodaya and ecological topics.

The Trust has conducted essay, speech and drawing competitions for children and youth and has awarded prizes to winners.

Trust has launched a ‘Pusthaka Koottayma’ ie a solidarity of Poornodaya magazine readers to whose members we give our books @ 50% subsidy and copy of our magazine free of cost. This association has 551 members.

In addition to all this Poornodaya Book Trust has played an active role in all the activities of Gandhian organizations in Kerala.

During a period of 23 years the Trust has made a total sale of books worth ` 3,93,16,521 (~4 Crores) giving maximum subsidy to public. It has helped to create an awareness about the life and message of Mahathma Gandhi in Kerala.
