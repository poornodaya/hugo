+++
title = 'Vision'
date = 2024-03-02T13:00:21-08:00
draft = false
+++

# Our Vision

Mahatma Gandhi will be well remembered in this century, as also in future; for he has contributed a rich heritage to the mankind for coexistence and survival.

If not politically, he will be quoted quite often. He gave us a philosophy coined as 'Sarvodaya', the simplest meaning is welfare of all. He based Truth and Non violence as the tenets of his vision for a Sarvodaya social order and wished to create a life style, where man can live harmoniously with the nature. He disowned the barriers of class, caste, creed, sex and nation. He believed that man can live peacefully on this earth and progress towards a better surroundings.

He has written much. Elucidated well on his teachings. His writings spread over 50000 Pages, and in volumes.

At Poornodaya Book Trust, while establishing in the year 1988, when Gandhian books were not easily available in Malayalam, we thought that Gandhian literature should be made available to the people of Kerala and undertook publishing books in Malayalam, so that even the common man can access and read them. Poornodaya Book Trust has brought, during these creative years about 129 titles and made available to the public.

We are glad and thankful that wide range of people have appreciated our efforts and cooperated with us during these years. That is the secret of our existence and success.
