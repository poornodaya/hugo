+++
title = 'About Us'
date = 2024-03-02T12:36:20-08:00
draft = false
+++

# About Poornodaya Book Trust

Poornodaya Book Trust is a public trust registered under Indian Trust Act. The Trust was formally inaugurated on 15th October 1988 by late Prof. M.P. Manmathan, a veteran Sarvodaya Leader. The aim of the trust is to propagate Gandhian ideas through publication and sale of books, seminars, study programmes, giving training to budding writers, exhibition of Gandhian books and pictures and holding discussion on various Gandhian and allied topics.

## OUR TEAM

**Chairman** : 	K.A. Chandran Ex MLA
**Vice Chairman** : 	T.R.N. Prabhu
**Secretary** : 	K. Parameswara Sarma
**Treasurer** : 	K. George Thomas

## Managing Committee Members

- R. Reghu
- C.N. Gopinathan
- R. Radhakrishnan
- Adv. S. Ramesh Babu
- Dr. Vincent Maliyekkal

## Founder Members

- Ajith Venniyoor
- T.O. George
- Dr. A.V. Bharathan
- K.P. Moosath T. Viswanathan
- George John
- M. Surendran Dr. K. Aravindakshan
- P.K. Harinarayanan

## Life Members

- Babubhai Thakker
- T.K. Somayya
- P. Gopinathan Nair
- Dr. Aarzoo
- Dr. N. Radhakrishnan
- Prof. M.Muhammed
- K.G. Jagadeesan
- Dr. C.K. Thomas
- M.V. Ramakrishnan
- Dr. S.K. Madhavan
- S. Asokan
- T.K. Achuthan
- Alok Prabhu
- G. Gopinathan Pillai
- C.N. Narayanan
- Prof. P. Narayana Menon
- G. Jayakrishnan
- Dr. P.A. Radhakrishnan
- K.V. Raghavan
- Prof. CherukunnamPurushothaman
- Rathankumar. R. Kamath
- V.C. Kabeer
- Prof. T.M. Surendranath
- T. Balakrishnan
- A.A. Gopalakrishnan
- M. Peethambaran
- A.K. Govindan
- Dr. P.S. Sreekantan Thampi
- Prof. Ponnara Saraswathi
- P.A. Mariamma
- T.P. Padmanabhan
- Davis Kannampuzhza
- Dr. C.V. Jayamani
- Dr. T. Mathew Philip
- A.K. Gangadharan
- Santhosh John Alappatt
- G. Pradeepkumar
- P. Krishnakumar
- Adv. Thampan Thomas Ex MP
- I.V. Anirudhan
- Adv. M.P. Sukumaran
- Jathavedan Namboothiri
- K.P. Manojkumar
- E.A. Balan
- Prof. V.P.G. Marar
- A.K.P. Narayanan
- P. Gangadharan
- Dr. P.V. Krishnan Nair
- Kottukal Krishnakumar
- Adv. T.K. Sudhakaran
- K.G. Anilkumar
- P. Ramachandran Nair
- Dr. D. Maya
- Mathew. M. Kandathil
- T.K.A. Azeez
- K. Manikandan
- Rugmini Ramakrishnan
- Dr. Jacob Pulickan
- Vijayaraghavan Cheliya
- P.V. Gangadharan
- K. Radhakrishnan
- S. Viswakuamran Nair
- O.V. Usha
- K.P. Premalatha Menon
- P.T. Thomas MLA
- Ramachandran. P.T
- K. Shyamasundar
